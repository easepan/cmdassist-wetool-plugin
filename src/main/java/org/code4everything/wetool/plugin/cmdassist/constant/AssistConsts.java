package org.code4everything.wetool.plugin.cmdassist.constant;

import lombok.experimental.UtilityClass;

/**
 * @author pantao
 * @since 2019/10/8
 */
@UtilityClass
public class AssistConsts {

    public static final String TAB_ID = "ease-cmd-assist";

    public static final String TAB_NAME = "命令助记器";
}
