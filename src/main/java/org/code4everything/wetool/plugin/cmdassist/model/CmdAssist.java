package org.code4everything.wetool.plugin.cmdassist.model;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.code4everything.boot.base.FileUtils;
import org.code4everything.boot.base.bean.BaseBean;
import org.code4everything.wetool.plugin.support.util.WeUtils;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pantao
 * @since 2019/10/8
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CmdAssist implements BaseBean, Serializable {

    private static final String PATH = "conf" + File.separator + "cmd-assist.json";

    private static final long serialVersionUID = -3740981276883857036L;

    private static final List<CmdAssist> EMPTY = new ArrayList<>();

    private String alias;

    private String cmd;

    private String remark;

    public static List<CmdAssist> loadConfig() {
        String path = WeUtils.parsePathByOs(PATH);
        if (StrUtil.isEmpty(path)) {
            return EMPTY;
        }
        String json = FileUtil.readUtf8String(path);
        try {
            return JSON.parseArray(json, CmdAssist.class);
        } catch (Exception e) {
            return EMPTY;
        }
    }

    public static String getPath() {
        return StrUtil.emptyToDefault(WeUtils.parsePathByOs(PATH), FileUtils.currentWorkDir(PATH));
    }
}
