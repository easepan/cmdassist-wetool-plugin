package org.code4everything.wetool.plugin.cmdassist;

import javafx.scene.Node;
import javafx.scene.control.MenuItem;
import org.code4everything.wetool.plugin.cmdassist.constant.AssistConsts;
import org.code4everything.wetool.plugin.support.WePluginSupporter;
import org.code4everything.wetool.plugin.support.util.FxUtils;

/**
 * @author pantao
 * @since 2019/10/8
 */
public class WetoolSupporter implements WePluginSupporter {

    @Override
    public MenuItem registerBarMenu() {
        return FxUtils.createBarMenuItem(AssistConsts.TAB_NAME, event -> initBootIfConfigured());
    }

    @Override
    public void initBootIfConfigured() {
        Node node = FxUtils.loadFxml(WetoolSupporter.class, "/ease/cmdassist/Main.fxml", true);
        FxUtils.openTab(node, AssistConsts.TAB_ID, AssistConsts.TAB_NAME);
    }
}
