package org.code4everything.wetool.plugin.cmdassist;

import org.code4everything.wetool.plugin.test.WetoolTester;

/**
 * @author pantao
 * @since 2019/10/8
 */
public class AssistTest {

    public static void main(String[] args) {
        WetoolTester.runTest(args);
    }
}
